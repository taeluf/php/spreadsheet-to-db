<?php

namespace Tlf;

/**
 * .ods files are NEVER overwritten (except in case of error, maybe? Do use version control)
 *
 */
function build_csv_conversions($options){
    $defaults = [ 
            // all settings shown are the defaults
            
            'source_dir' => __DIR__.'/data/data/',
            //relative path to source_dir
            'db_rel_path'=> '../data.sqlite',

            'path_prefix'=>__DIR__.'/data/raw-',

            // bugfix: Fileformat encoding leaves some characters incompatible with utf8, so apply a fix with libreoffice cli
            'use_filetype_conversion'=>true,

            // WARNING: OVERWRITES FILES if true
            // none of the files should be edited, generally. They're meant to be built from .ods spreadsheets
            // Good to set them false if testing some kind of error
            //
            'gen_csv'=>true,
            'gen_json'=>true,
            'gen_sql'=>true,

            'gen_sqlite'=>true,

        ];
    // expose options & defaults as variables
    foreach ($defaults as $key=>$default){
        if (isset($options[$key]))$$key = $options[$key];
        else $$key = $default;
    }

    $dir = dirname(__DIR__);

    $odsDir = $source_dir;
    $csvDir = $path_prefix.'csv/';
    $jsonDir = $path_prefix.'json/';
    $sqlDir = $path_prefix.'sql/';
    $databasePath = $source_dir.'/'.$db_rel_path;


    if (!is_dir($odsDir)){
        echo "\n\nsource dir '$odsDir' does not exist";
    }

    if ($gen_csv){
        echo "\n\n";
        echo "    ++ .ods to csv ++";
        echo "\n\n\n";
        if (!is_dir($csvDir)){mkdir($csvDir);}
        export_to_csv($odsDir, $csvDir);
        // echo "\n--\n";
        // echo ".ods to csv DONE";
        // echo "\n--\n\n";
    }


    if ($gen_json){
        echo "\n\n";
        echo "   ++ csv to json ++";
        echo "\n\n\n";
        if (!is_dir($jsonDir)){mkdir($jsonDir);}
        csv_to_clean_json($csvDir, $jsonDir);
        // echo "\n--\n";
        // echo "csv to json DONE";
        // echo "\n--\n\n";
    }

    if ($gen_sql){
        echo "\n\n";
        echo "    ++ json to sql ++";
        echo "\n\n\n";
        if (!is_dir($sqlDir))mkdir($sqlDir);
        json_to_sql($jsonDir, $sqlDir);
        // echo "\n--\n";
        // echo "json to sql DONE";
        // echo "\n--\n\n";
    }

    if ($gen_sqlite){
        echo "\n\n";
        echo "    ++ execute sql, create .sqlite dbs ++";
        echo "\n\n\n";
        if (!is_dir($sqlDir))mkdir($sqlDir);
        if (!is_dir($sqlDir))mkdir($sqlDir);
        if (!is_dir($dbDir=dirname($databasePath))){
            throw new \Exception("$dbDir does not exist. Cannot save '$databasePath'");
        }
        execute_sql($sqlDir, $databasePath);
    }

    echo "\n\n";
}
