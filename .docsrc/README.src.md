# Spreadsheet To DB (& other conversions)
Convert .ods files into .csv, .json, .sql, and create an .sqlite database

## Install
@template(composer_install, taeluf/spreadsheet-converter)

### Dependencies
- libreoffice (We convert .ods files with the `libreoffice` cli command)

## Usage
See `test/example.php` and `test/data` to for a working example. You can execute `test/example.php` to make sure it works on your system.

1. Make a php file like this 
2. `chmod ug+x the-file.php` to make it executable.
3. `./the-file.php` to execute it & build your database

```php
@file(test/example.php)
```

### File Structure
The root dir can be anything, I'm using data for the example
```
data/
    src/ -- all the data that you enter manually
        covid-cases.ods
        local-use-of-force-incidents.ods
        local-traffic-stop-disparities.ods
        delicious-gummi-bears.ods
    raw-csv/
        local_use_of_force_incidents.csv
        -- and the other files
        -- these are generated from the .ods files
    raw-json/
        -- same
    raw-sql/
        -- same
    data.sqlite -- is overwritten on build & tables are named after their source files 
                -- so delicious_gummi_bears is a table (hyphens replaced with underscore)
```

